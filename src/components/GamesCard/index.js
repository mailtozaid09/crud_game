import React from 'react'
import './gamesCard.css';

import moment from 'moment';

const GamesCard = ({item}) => {
    return (
        <div className='games_card_container'>
            <img src={item.image} alt={item.name} className="image_style" />

            <div className='content_container' >
                <div className='game_name' >{item.name}</div>
                <div className='game_author' >{item.author}</div>
                <div className='game_date' >{moment(item.published_date).format('Do MMMM, YYYY')}</div>
            </div>

        </div>
    )
}

export default GamesCard