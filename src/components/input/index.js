import React from 'react'
import { BsEyeFill } from 'react-icons/bs';
import './input.css';


const Input = ({title, placeholder, onChange, value, icon,showPass, onShowPass }) => {
    return (
        <div>
            <div className='input_label' >{title ? title : 'Input Label'}</div>
            <div className='input_icon_container' >
                <input
                    className='input_container'
                    placeholder={placeholder}
                    title={title}
                    type={icon ? !showPass ? "password" : "text" : "text"}
                    name={title}
                    value={value}
                    onChange={onChange}
                />
            {icon ? <div className='input_icon' onClick={onShowPass} > {icon}</div> : null}
            </div>
        </div>
    )
}

export default Input