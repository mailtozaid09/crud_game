import React from 'react'

import './button.css'

const Button = ({title, onPress, icon, type }) => {
    return (
        <div className={type == 'delete' ? 'delete_button_style' : 'button_style'}
            onClick={onPress}
        >
            <div>{icon}</div>
            <h4>{title ? title : 'Button'}</h4>
        </div>
    )
}

export default Button