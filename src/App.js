import React, { useState, useEffect } from "react";

import { BrowserRouter, Routes, Route, Link, Navigate } from "react-router-dom";
import CreateScreen from "./screens/create";
import HomeScreen from "./screens/home";
import LoginScreen from "./screens/login";
import ReadScreen from "./screens/read";
import UpdateScreen from "./screens/update";



const App = () => {
	const [isLogin, setIsLogin] = useState(true);


	useEffect(() => {

	}, []);

  	return (
		<BrowserRouter>
			<Routes>
				<Route exact path='/' element={<LoginScreen />} />
				<Route path='/home' element={<HomeScreen />} />
				<Route path='/create' element={<CreateScreen />} />
				<Route path='/read' element={<ReadScreen />} />
				<Route path='/update' element={<UpdateScreen />} />
			</Routes>
		</BrowserRouter>
	);
};

export default App;
