import React, {useState, useEffect} from 'react';

import './home.css'

import GamesCard from '../../components/GamesCard';
import Button from '../../components/buttons';

import { useNavigate } from "react-router-dom";
import { IoMdAddCircle } from 'react-icons/io';


// 	const arr = [
	// 		{
	// 			"name": "Bottle Flip",
	// 			"url": "https://simpleiralgames.com",
	// 			"author": "Simple Viral Games",
	// 			"published_date": "2022-08-01 00:00:00"
	// 		},
	// 		{
	// 			"name": "Bottle Flip",
	// 			"url": "https://simpleiralgames.com",
	// 			"author": "Simple Viral Games",
	// 			"published_date": "2022-08-01 00:00:00"
	// 		},
	// 		{
	// 			"name": "Bottle Flip",
	// 			"url": "https://simpleiralgames.com",
	// 			"author": "Simple Viral Games",
	// 			"published_date": "2022-08-01 00:00:00"
	// 		},
				
	// 	];

const HomeScreen = () => {
    const navigate = useNavigate();

	const [gamesArray, setGamesArray] = useState([]);

	 useEffect(() => {
		const str = localStorage.getItem("array");
		const parsedArr = JSON.parse(str);
		setGamesArray(parsedArr)
		console.log(parsedArr);

	}, [])

	
	
	return (
		<div className='container' >
		
			
			<div className='main_container' >

				<h1>CRUD Game</h1>
					
				<div className='heading_container' >
					<h2>All Games</h2>
					<Button 
						title="Add Game"
						onPress={() => {navigate("/create");}}
						icon={<IoMdAddCircle size={26} />}
					/>
					
				</div>

				<div>
					{gamesArray == null || gamesArray && gamesArray.length == 0 ?
						<div className='empty_list_container' >
							<h2>No Games Found</h2>
							<img src={require('../../assets/empty_image.png')} className="empty_list_img" alt="no_result_found" />

							<h3>Please add new game!</h3>
						</div>
					:
					null
					}
				</div>

				<div className='home_container' >
					{gamesArray && gamesArray.map((item) => (
						<div
							onClick={() => {
								navigate('/read',{
									state:  {
										game: item,
									}
								});
							}}
						>
							<GamesCard 
								item={item}
							/>
						</div>
					))}
				</div>

				
			</div>
		</div>
	);
}
export default HomeScreen;
