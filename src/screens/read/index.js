import React, {useState, useEffect} from 'react';

import './read.css'
import moment from 'moment';
import { useLocation, useNavigate } from 'react-router-dom';
import Button from '../../components/buttons';


import { RiEdit2Fill } from 'react-icons/ri';
import { MdDelete } from 'react-icons/md';
import Swal from 'sweetalert2';



const ReadScreen = ({}) => {

    const navigate = useNavigate();

    const location = useLocation();

    const [gameDetails, setGameDetails] = useState(location.state.game);

	useEffect(() => {


	}, [])


    const deleteFunction = () => {

        const array = localStorage.getItem("array");
		const gamesArr = JSON.parse(array);


        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#000',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {

                const deleteGame = gamesArr.filter((game) => game.name !== gameDetails.name);
   
                localStorage.setItem("array", JSON.stringify(deleteGame));

                Swal.fire(
                    'Deleted!',
                    'You have successfully deleted a game!',
                    'success'
                )
                navigate('/home')
            }
          })

      
    }

	
	return (
		<div className='container' >
			<div className='card_container' >
                <h2>Game Details</h2>

                
                <div className='details_container'>
                    <img src={gameDetails.image} alt={gameDetails.name} className="read_image_style" />

                    <div className='content_container' >
                        <div className='game_name' >Name: {gameDetails.name}</div>
                        <div className='game_author' >URL: {gameDetails.url}</div>
                        <div className='game_author' >Author: {gameDetails.author}</div>
                        <div className='game_date' >Published Date: {moment(gameDetails.published_date).format('Do MMMM, YYYY')}</div>
                    </div>

                </div>
                
                <div className='button_container' >
                    <div>
                        <Button 
                            title="Delete Game"
                            onPress={() => {deleteFunction(gameDetails)}}
                            icon={<MdDelete size={26} color="red" />}
                            type="delete"
                        />
                    </div>


                    <div>
                        <Button 
                            title="Update Game"
                            onPress={() => {
                                navigate('/update',{
                                    state:  {
                                        game: gameDetails,
                                    }
                                });
                            }}
                            icon={<RiEdit2Fill size={26} />}
                        />
                    </div>
                </div>
            </div>
		</div>
	);
}
export default ReadScreen;
