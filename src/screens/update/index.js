import React, {useState, useEffect} from 'react';

import './update.css'


import Input from '../../components/input';
import Button from '../../components/buttons';

import DateTimePicker from 'react-datetime-picker';
import { useLocation, useNavigate } from 'react-router-dom';

import { IoMdAddCircle } from 'react-icons/io';
import Swal from 'sweetalert2';


const UpdateScreen = () => {

    const navigate = useNavigate();
    const location = useLocation();

    const [gameDetails, setGameDetails] = useState(location.state.game);


    const [form, setForm] = useState(location.state.game);
    const [calendarValue, setCalendarValue] = useState(location.state.game.published_date);
    

	useEffect(() => {
        setForm({ 
            ['Name']: gameDetails.name,
            ['URL']: gameDetails.url,
            ['Author']: gameDetails.author
        })
	}, [])

    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
    };


    const checkIsFormFilled = () => {

        var isFilled = true;

        if(!calendarValue){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter the published date of the your game!'
            })
        }

        if(!form.Author){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter the author of the your game!'
            })
        }

        if(!form.URL){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter the url of the your game!'
            })
        }

        if(!form.Name){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter the name of the your game!'
            })
        }

        return isFilled;

    }


    const createFunction = () => {

        if(checkIsFormFilled()){
            console.log("form filled ?? =>>> yesss");
            console.log("form => " , form);

            let data = JSON.parse(localStorage.getItem("array"));
            const myData = data.map(x => {
                if (x.name === gameDetails.name) {
                    return {
                        ...x,
                        name: form.Name,
                        url: form.URL,
                        author: form.Author,
                        published_date: calendarValue,
                    }
                }
                return x;
            })
      
            localStorage.setItem("array", JSON.stringify(myData));

            Swal.fire({
                icon: 'success',
                title: 'Yay...',
                text: 'You have successfully updated an existing game!',
                showConfirmButton: false,
                timer: 2000
            })

            navigate('/home')
        }


    }
	
	return (
		<div className='container' >
			<div className='card_container' >
                <h2>Update Game</h2>

                <Input 
                    title="Name"
                    placeholder="Enter the name of game"
                    value={form.Name}
                    onChange={event => onChange({name: 'Name', value: event.target.value})}
                />

                <Input 
                    title="URL"
                    value={form.URL}
                    placeholder="Enter the url of game"
                    onChange={event => onChange({name: 'URL', value: event.target.value})}
                />

                <Input 
                    title="Author"
                    value={form.Author}
                    placeholder="Enter the author of game"
                    onChange={event => onChange({name: 'Author', value: event.target.value})}
                />     

                <div className='date_container' >
                    <label className='date_label' >
                        Published Date
                    </label>
                    <DateTimePicker 
                        dayPlaceholder="dd"
                        monthPlaceholder="mm"
                        yearPlaceholder="yyyy"
                        minutePlaceholder="mm"
                        hourPlaceholder="hh"
                        onChange={setCalendarValue} value={calendarValue} />
                </div>

                <div>
                    <Button
                        title="Update Game"
                        onPress={() => {
                            createFunction()
                        }}
                        icon={<IoMdAddCircle size={26} />}
                    />
                </div>

            </div>
		</div>
	);
}
export default UpdateScreen;
