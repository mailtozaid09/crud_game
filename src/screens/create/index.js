import React, {useState, useEffect} from 'react';

import './create.css'


import Input from '../../components/input';
import Button from '../../components/buttons';

import DateTimePicker from 'react-datetime-picker';
import { useNavigate } from 'react-router-dom';

import dummyImg1 from '../../assets/dummy1.png'
import dummyImg2 from '../../assets/dummy2.png'
import dummyImg3 from '../../assets/dummy3.png'
import dummyImg4 from '../../assets/dummy4.png'
import dummyImg5 from '../../assets/dummy5.png'
import dummyImg6 from '../../assets/dummy6.png'
import dummyImg7 from '../../assets/dummy7.png'
import dummyImg8 from '../../assets/dummy8.png'
import dummyImg9 from '../../assets/dummy9.png'
import dummyImg10 from '../../assets/dummy10.png'

import { IoMdAddCircle } from 'react-icons/io';

import Swal from "sweetalert2"


const CreateScreen = () => {

    const navigate = useNavigate();

    const [form, setForm] = useState({});

    const [calendarValue, setCalendarValue] = useState(new Date());
    

    const dummyUserImg = ['img1', 'img2', 'img3', 'img4', 'img5', 'img6', 'img7', 'img8', 'img9', 'img10' ]

    const dummyImg = dummyUserImg[(Math.random() * dummyUserImg.length) | 0]


	useEffect(() => {


	}, [])

    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
    };


    const checkIsFormFilled = () => {

        var isFilled = true;

        if(!calendarValue){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter the published date of the your game!'
            })
        }

        if(!form.Author){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter the author of the your game!'
            })
        }

        if(!form.URL){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter the url of the your game!'
            })
        }

        if(!form.Name){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter the name of the your game!'
            })
        }

        return isFilled;

    }


    const createFunction = () => {

        if(checkIsFormFilled()){
            console.log("form filled ?? =>>> yesss");
            console.log("form => " , form);

            var newGame = {
                "name": form.Name,
                "url": form.URL,
                "author": form.Author,
                "published_date": calendarValue,
                "image": 
                dummyImg == 'img1' ? dummyImg1 :
                dummyImg == 'img2' ? dummyImg2 :
                dummyImg == 'img3' ? dummyImg3 :
                dummyImg == 'img4' ? dummyImg4 :
                dummyImg == 'img5' ? dummyImg5 :
                dummyImg == 'img6' ? dummyImg6 :
                dummyImg == 'img7' ? dummyImg7 :
                dummyImg == 'img8' ? dummyImg8 :
                dummyImg == 'img9' ? dummyImg9 :
                dummyImg == 'img10' ? dummyImg10 :
                dummyImg1
            }

            const games = localStorage.getItem("array");
            const parsedGames = JSON.parse(games);

            console.log("parsedGamesparsedGames => ", parsedGames);

            var newGames;

            if(parsedGames == null){
                newGames = JSON.stringify([newGame])
            }else{
                newGames = JSON.stringify([...parsedGames, newGame])
            }
            
            
            
            localStorage.setItem("array", newGames);

            Swal.fire({
                icon: 'success',
                title: 'Yay...',
                text: 'You have successfully added a new game!',
                showConfirmButton: false,
                timer: 2000
            })

            navigate('/home')
        }else{
            console.log("form filled =>>> noooo");
        }
    }
	
	return (
		<div className='container' >
			<div className='card_container' >
                <h2>Create Game</h2>

                <Input 
                    title="Name"
                    placeholder="Enter the name of game"
                    onChange={event => onChange({name: 'Name', value: event.target.value})}
                />

                <Input 
                    title="URL"
                    placeholder="Enter the url of game"
                    onChange={event => onChange({name: 'URL', value: event.target.value})}
                />

                <Input 
                    title="Author"
                    placeholder="Enter the author of game"
                    onChange={event => onChange({name: 'Author', value: event.target.value})}
                />     

                <div className='date_container' >
                    <label className='date_label' >
                        Published Date
                    </label>
                    <DateTimePicker 
                        dayPlaceholder="dd"
                        monthPlaceholder="mm"
                        yearPlaceholder="yyyy"
                        minutePlaceholder="mm"
                        hourPlaceholder="hh"
                        onChange={setCalendarValue} value={calendarValue} />
                </div>

                <div>
                    <Button
                        title="Create Game"
                        onPress={() => {
                            createFunction()
                        }}
                        icon={<IoMdAddCircle size={26} />}
                    />
                </div>

            </div>
		</div>
	);
}
export default CreateScreen;
