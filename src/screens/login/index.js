import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Input from '../../components/input'
import './login.css'



import { BsEyeFill, BsEyeSlashFill } from 'react-icons/bs';

const LoginScreen = () => {

    const navigate = useNavigate();

    const [form, setForm] = useState({});

    const [userDetails, setUserDetails] = useState({
        email: 'test@gmail.com',
        password: 'testpass'
    });

    const [showPass, setShowPass] = useState(false);    


    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
    };


    const checkIsFormFilled = () => {

        var isFilled = true;

        if(!form.Password){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter your password!' 
            })
        }

        if(!form.Email){
            isFilled = false
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Enter your email address!'
            })
        }

        return isFilled;

    }


    const loginFunction = () => {
        
        if(checkIsFormFilled() && form.Email == userDetails.email &&  form.Password == userDetails.password){
            console.log("form filled ?? =>>> yesss");
            console.log("form => " , form);

            Swal.fire({
                icon: 'success',
                title: 'Yay...',
                text: 'You have successfully logged in!',
                showConfirmButton: false,
                timer: 2000
            })

            navigate('/home')
        }else{
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Wrong email address or password!'
            })
        }
        
    }



    return (
        <div className="login_container">
            <div className='login_image_container' >
                <div>
                    <img src={require('../../assets/login_image.png')} className="login_image_style" alt="login" />
                </div>
            </div>
            <div className='login_content_container' >

                <h1>CRUD Game</h1>

                <div className='login_heading_container' >
                    <div className='login_title' >Hello! Welcome back.</div>
                    <div className='login_subtitle' >Login with the credentials that you entered during your registration.</div>
                </div>
                <Input 
                    title="Email"
                    showPass={true}
                    icon={<BsEyeSlashFill size={26} color="#f6f8fa" />}
                    placeholder="Example@gmail.com"
                    onChange={event => onChange({name: 'Email', value: event.target.value})}
                />

                <Input 
                    icon={showPass ? <BsEyeFill size={26} /> : <BsEyeSlashFill size={26} />}
                    title="Password"
                    placeholder="Enter password"
                    onChange={event => onChange({name: 'Password', value: event.target.value})}
                    showPass={showPass}
                    onShowPass={() => setShowPass(!showPass)}
                />


                <button className='login_button' onClick={() => loginFunction()} >
                    Login
                </button>
            </div>
        </div>
       
    )
}

export default LoginScreen